# Example usage of ORM layer
# be sure to run proj/setup-db.py if you haven't already!
import copy

import rethinkdb as r

from proj.models import models

# this will be injected into Flask routes, usually can be accessed as `g.rdb_conn`
conn = r.connect(db='myth')


# player deity
p_deity = models.Deity(name='Zeus',
                       moves=[models.Move('Bolt', 10, 5, models.ElementalType('electric'))],
                       stats=models.Stats(5, 2, 5, 50),
                       types=[models.ElementalType('electric')]
                       )

# any database operations MUST called with .run or else the changes won't be committed.
p_deity.save().run(conn)

# cpu deity
c_deity = models.Deity(name="Zeus's Twin Brother",
                       moves=[models.Move('Bolt', 10, 5, models.ElementalType('electric'))],
                       stats=models.Stats(5, 2, 5, 50),
                       types=[models.ElementalType('electric')]
                       )
c_deity.save().run(conn)

g = models.GameInstance(game_states=[])
initial_state = models.GameState(player_deity=p_deity, cpu_deity=c_deity, turn=0)
g.game_states.append(initial_state)
g.save().run(conn)

# imagine that both players have attacked now...
new_state: models.GameState = copy.deepcopy(g.latest_state)

new_state.player_deity.moves[0].number_of_uses -= 1
new_state.cpu_deity.moves[0].number_of_uses -= 1

new_state.player_deity.stats.health -= 5
new_state.cpu_deity.stats.health -= 5

g.game_states.append(new_state)
response = g.save().run(conn)

game_id = response['generated_keys'][0]
# now let's say we want to retrieve this game state at a later time...
temp = models.GameInstance.objects.get(game_id).run(conn)  # don't forget run!
# temp is a dictionary object which we can serialize into a model object
g = models.GameInstance.from_dict(temp)
print(g)  # voila, ready to use
