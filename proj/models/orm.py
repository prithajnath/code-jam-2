import copy
from abc import ABC, abstractclassmethod, abstractmethod

import rethinkdb as r


class Serializable(ABC):
    """
    Used to represent a serializable object
    """

    @abstractclassmethod
    def from_dict(self, dct: dict) -> 'Model':
        """
        create an instance of Model subclass using provided dictionary
        :param dct: dict representation of model instance
        :return: instance of Model subclass
        """
        pass

    @abstractmethod
    def to_dict(self) -> dict:
        """
        creates a dict that represents an instance of a Model subclass
        :return: dict representation of object
        """
        pass


class Model(Serializable):
    """
    Used as base class for user-defined model types to support serialization.
    """
    table_name: str
    objects: 'ORMInstance'

    def save(self):
        """
        Creates or updates the Model subclass based on if the object has an `db_id` attribute.
        :return r.RqlQuery
        """
        if getattr(self, 'db_id', None) is not None:
            query = r.table(self.table_name).get(self.db_id).update(self.to_dict())
        else:
            query = r.table(self.table_name).insert(self.to_dict())
        return query

    def copy(self):
        """
        Creates a deep copy of obj and removes its `db_id` attribute so it will be saved as a new row in the database.
        :return: deep copy of object
        """
        obj = copy.deepcopy(self)
        if hasattr(obj, 'db_id'):
            delattr(obj, 'db_id')
        return obj


class ORMInstance:
    def __init__(self, table_name: str):
        """
        Ties the subclass to a particular table in database
        :param table_name: name of table in database
        """
        self.table_name = table_name

    def filter(self, key: str, value: str) -> r.RqlQuery:
        """
        filter db by row in the subclass's table
        :param key: row-name in database
        :param value: value to filter by
        :return: database query which can then be executed by `return_value.run(conn)`
        """
        return r.table(self.table_name).filter(r.row[key] == value)

    def all(self) -> r.RqlQuery:
        """
        returns all instances of object in subclass's table
        :return: database query which can then be executed by `return_value.run(conn)`
        """
        return r.table(self.table_name)

    def get(self, db_id):
        """
        Returns a singular instance of this Model subclass based on it's id in the database
        :param db_id: object's id in the database
        :return: r.RqlQuery
        """
        return r.table(self.table_name).get(db_id)
