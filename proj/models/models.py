from proj.models.orm import Model, ORMInstance, Serializable

TABLES_NEEDED = ['deities', 'games']


class ElementalType(Serializable):
    """
    Used to represent the type of a Deity or Move, such as fire, water, etc.
    """

    def __init__(self, typename: str):
        """
        Create a new ElementalType
        :param typename: name of type e.g. "fire"
        """
        self.typename = typename

    def to_dict(self) -> dict:
        return {'typename': self.typename}

    @classmethod
    def from_dict(cls, dct: dict) -> 'ElementalType>':
        return ElementalType(dct['typename'])

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__} : {self.typename}>'


class Stats(Serializable):
    """
    Used to hold the various stats of a Deity
    """
    def __init__(self, attack: int, defense: int, speed: int, health: int):
        """
        Create new Stats instance
        :param attack: attack modifier
        :param defense: defense modifier
        :param speed: speed modifier
        :param health: amount of health points
        """
        self.attack = attack
        self.defense = defense
        self.speed = speed
        self.health = health

    @classmethod
    def from_dict(cls, dct: dict) -> 'Stats':
        return Stats(attack=dct['attack'], defense=dct['defense'], speed=dct['speed'], health=dct['health'])

    def to_dict(self) -> dict:
        return {'attack': self.attack, 'defense': self.defense, 'speed': self.speed, 'health': self.health}

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__} : {self.attack}, {self.defense}, {self.speed}, {self.health}>'


class Move(Serializable):
    """
    Used to represent a move that can be used by a Deity
    """
    def __init__(self, name: str, power: int, number_of_uses: int, damage_type: ElementalType):
        """
        Create new Move instance
        :param name: name of move
        :param power: damage modifier of move
        :param number_of_uses: number of uses in battle
        :param damage_type: ElementalType of Move
        """
        self.name = name
        self.power = power
        self.number_of_uses = number_of_uses
        self.damage_type = damage_type

    @classmethod
    def from_dict(cls, dct: dict) -> 'Stats':
        damage_type = ElementalType.from_dict(dct['damage_type'])
        obj = Move(name=dct['name'], power=dct['power'], number_of_uses=dct['number_of_uses'], damage_type=damage_type)
        return obj

    def to_dict(self) -> dict:
        damage_type = ElementalType.to_dict(self.damage_type)
        return {'name': self.name,
                'power': self.power,
                'number_of_uses': self.number_of_uses,
                'damage_type': damage_type
                }

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__} : {self.name}>'


class Deity(Model):
    """
    Used to represent a Deity, the characters the player and CPU control.
    """
    table_name = 'deities'
    objects = ORMInstance(table_name)

    def __init__(self, name: str, moves: [Move], stats: Stats, types: [ElementalType]):
        """
        Create new Deity instance
        :param name: name of Deity
        :param moves: collection of moves of Deity
        :param stats: base stats
        :param types: ElementalTypes of Deity used to determine strength of attacks against it
        """
        self.name = name
        self.moves = moves
        self.stats = stats
        self.types = types

    @classmethod
    def from_dict(cls, dct: dict) -> 'Deity':
        moves = [Move.from_dict(move) for move in dct['moves']]
        stats = Stats.from_dict(dct['stats'])
        types = [ElementalType.from_dict(type) for type in dct['types']]
        obj = Deity(name=dct['name'], moves=moves, stats=stats, types=types)
        obj.__dict__['db_id'] = dct.get('id', None)
        return obj

    def to_dict(self) -> dict:
        return {
            'name': self.name,
            'moves': [move.to_dict() for move in self.moves],
            'stats': self.stats.to_dict(),
            'types': [type.to_dict() for type in self.types],
        }


class GameState(Serializable):
    """
    Used to represent a particular turn in battle.
    """
    def __init__(self, player_deity: Deity, cpu_deity: Deity, turn: int):
        """
        Create new GameState instance
        :param player_deity: the mutable deity used by the player
        :param cpu_deity: the mutable deity used by the CPU
        :param turn: turn index
        """
        self.player_deity = player_deity.copy()
        self.cpu_deity = cpu_deity.copy()
        self.turn = turn

    @classmethod
    def from_dict(cls, dct: dict) -> 'GameState':
        player_deity = Deity.from_dict(dct['player_deity'])
        cpu_deity = Deity.from_dict(dct['cpu_deity'])
        return GameState(player_deity=player_deity, cpu_deity=cpu_deity, turn=dct['turn'])

    def to_dict(self) -> dict:
        player_deity = self.player_deity.to_dict()
        cpu_deity = self.cpu_deity.to_dict()
        return {'player_deity': player_deity, 'cpu_deity': cpu_deity, 'turn': self.turn}


class GameInstance(Model):
    """
    Used to contain GameStates into a single game.

    Proposed usage of this model is to use the computed property GameInstance.latest_state by copying it using
    copy.deepcopy and then appending it to GameInstance.game_states once modifications have been made for the
    current turn.
    """
    table_name = 'games'
    objects = ORMInstance(table_name)

    def __init__(self, game_states: [GameState]):
        """
        Create a new GameInstance instance.
        :param game_states: array of GameState objects
        """
        self.game_states = game_states

    @property
    def latest_state(self):
        """
        Gives the most recent state
        :return: GameState object
        """
        return self.game_states[-1]

    @classmethod
    def from_dict(cls, dct: dict) -> 'GameInstance':
        states = [GameState.from_dict(state) for state in dct['states']]
        obj = GameInstance(game_states=states)
        obj.__dict__['db_id'] = dct.get('id', None)
        return obj

    def to_dict(self) -> dict:
        game_states = [state.to_dict() for state in self.game_states]
        return {'states': game_states}
