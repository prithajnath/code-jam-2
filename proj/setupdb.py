import os

import rethinkdb as r
from rethinkdb.errors import RqlRuntimeError

from proj.models import models

RETHINK_HOST = os.environ.get('RETHINK_HOST') or 'localhost'
RETHINK_PORT = os.environ.get('RETHINK_PORT') or 28015
MYTH_DB = 'myth'


def setup():
    conn = r.connect(host=RETHINK_HOST, port=RETHINK_PORT)
    try:
        r.db_create(MYTH_DB).run(conn)
        for table in models.TABLES_NEEDED:
            r.db(MYTH_DB).table_create(table).run(conn)
    except RqlRuntimeError:
        print('database is already setup')


if __name__ == '__main__':
    setup()
