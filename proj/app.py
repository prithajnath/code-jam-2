import os

import rethinkdb as r
from flask import Flask, abort, g
from flask_restful import Api, Resource
from rethinkdb.errors import RqlDriverError

from proj.models import models

RETHINK_HOST = os.environ.get('RETHINK_HOST') or 'localhost'
RETHINK_PORT = os.environ.get('RETHINK_PORT') or 28015
MYTH_DB = 'myth'

app = Flask(__name__)
api = Api(app)


@app.before_request
def get_connection():
    try:
        g.rdb_conn = r.connect(host=RETHINK_HOST, port=RETHINK_PORT, db=MYTH_DB)
    except RqlDriverError:
        abort(503, "Couldn't establish database connection")


@app.teardown_request
def close_connection(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


class Hello(Resource):
    def get(self):
        return {'hello': 'world'}


class ListDeities(Resource):
    def get(self):
        deities_list = list(models.Deity.objects.all().run(g.rdb_conn))
        # this is an unneeded conversion just to show how ORM works, can easily return deities_list
        deity_objs = [models.Deity.from_dict(deity) for deity in deities_list]
        return [deity.to_dict() for deity in deity_objs]


api.add_resource(Hello, '/')
api.add_resource(ListDeities, '/deities')


def main():
    app.run()


if __name__ == '__main__':
    main()
