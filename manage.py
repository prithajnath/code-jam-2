#!/usr/bin/env python3
import sys

from proj.app import main as runserver
from proj.setupdb import setup as setupdb

if len(sys.argv) != 2:
    print("Invalid number of arguments")
else:
    keyword = str(sys.argv[1]).lower()
    if keyword == "setupdb":
        setupdb()
    elif keyword == "runserver":
        runserver()
